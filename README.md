# Java examples for data.europa.eu Search API


## Examples

To see an interactive demo, you can open the [index.html](index.html) file in your browser. 

### Search

To search for a term, the Endpoint `/search` has to be used with the parameter `q`.

[search_fetch.js](js/search_fetch.js):
```javascript
function search_fetch() {

    // search for the term 'Covid 19'

    fetch("https://data.europa.eu/api/hub/search/search?q=covid%2019", {
        "method": "GET"
    })
        .then(async response => {
            const json = await response.json();
            let tb = document.getElementById("search")

            console.log("search for the term 'Covid 19'")
            console.log(json);
        })
        .catch(err => {
            console.error(err);
        });
}
```

### Search in specific catalogue

To limit the search to a specific catalogue, the `facets` query parameter has to be used with the catalog facet.
Additionally, to enable the facet search, the query parameter `filter` has to be set to `datasets`:

[searchWithFilter_fetch.js](js/searchWithFilter_fetch.js):
```javascript
function searchWithFilter_fetch() {

    // search for the term 'Covid 19' in the catalogue 'EU institutions',
    // which has the id 'european-union-open-data-portal'

    const endoint ="https://data.europa.eu/api/hub/search/search"
    const searchparam = "q=covid%2019"
    const facets = "facets=%7B%22catalog%22%3A%5B%22european-union-open-data-portal%22%5D%7D"
    const filter ="filter=dataset"

    fetch(endoint+ "?"+ searchparam+"&"+facets +"&" + filter, {
        "method": "GET"
    })
        .then(async response => {
            const json = await response.json();
            console.log("search for the term 'Covid 19' in the catalogue 'EU institutions',")
            console.log(json);
        })
        .catch(err => {
            console.error(err);
        });
}
```

### Retrieve a specific catalogue

To retrieve the information for a specific catalogue, the endpoint `/catalogues` has to be used with the catalogue ID in the path.

[catalogue_fetch.js](js/catalogue_fetch.js):
```javascript
function catalogue_fetch() {


    // retrieve the information for the catalogue 'EU institutions',
    // which has the ID 'european-union-open-data-portal'

    fetch("https://data.europa.eu/api/hub/search/catalogues/european-union-open-data-portal", {
        "method": "GET"
    })
        .then(async response => {
            const json = await response.json();

            console.log(json);
        })
        .catch(err => {
            console.error(err);
        });
}
```

### Retrieve a specific dataset

To retrieve the information for a specific dataset, the endpoint `/datasets` has to be used with the dataset ID in the path.

[dataset_fetch.js](js/dataset_fetch.js):
```javascript
function dataset_fetch() {


    // retrieve the information for a datasts in the catalogue 'EU institutions'
    // the dataset has the ID 'g3ebxpyzj2lulbjqwkhg'

    fetch("https://data.europa.eu/api/hub/search/datasets/g3ebxpyzj2lulbjqwkhg", {
        "method": "GET"
    })
        .then(async response => {
            const json = await response.json();

            console.log(json);
        })
        .catch(err => {
            console.error(err);
        });
}
```
