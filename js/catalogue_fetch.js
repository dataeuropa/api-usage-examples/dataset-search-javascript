function catalogue_fetch() {


    // retrieve the information for the catalogue 'EU institutions',
    // which has the ID 'european-union-open-data-portal'

    fetch("https://data.europa.eu/api/hub/search/catalogues/european-union-open-data-portal", {
        "method": "GET"
    })
        .then(async response => {
            const json = await response.json();

            console.log(json);
        })
        .catch(err => {
            console.error(err);
        });
}