function search_fetch() {

    // search for the term 'Covid 19'

    fetch("https://data.europa.eu/api/hub/search/search?q=covid%2019", {
        "method": "GET"
    })
        .then(async response => {
            const json = await response.json();
            let tb = document.getElementById("search")

            console.log("search for the term 'Covid 19'")
            console.log(json);
        })
        .catch(err => {
            console.error(err);
        });
}