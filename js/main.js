async function loadScripts() {
    const listings = document.getElementsByClassName("script");

    for (let elem of listings) {
        const file = "js/" + elem.dataset.file + ".js";
        await fetch(file, {
            "method": "GET"
        }).then(async response => {
            const stat = response.status;
            if(stat===404){
                elem.innerHTML = "Could not load " + file+ ". Please make sure, that the javascript code is in the correct location."
            }else{
                elem.innerHTML = await response.text();
            }

        })
            .catch(err => {

                console.error(err);
            });

    }
    hljs.highlightAll();
}
