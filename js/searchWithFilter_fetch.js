function searchWithFilter_fetch() {

    // search for the term 'Covid 19' in the catalogue 'EU institutions',
    // which has the id 'european-union-open-data-portal'

    const endoint ="https://data.europa.eu/api/hub/search/search"
    const searchparam = "q=covid%2019"
    const facets = "facets=%7B%22catalog%22%3A%5B%22european-union-open-data-portal%22%5D%7D"
    const filter ="filter=dataset"

    fetch(endoint+ "?"+ searchparam+"&"+facets +"&" + filter, {
        "method": "GET"
    })
        .then(async response => {
            const json = await response.json();
            console.log("search for the term 'Covid 19' in the catalogue 'EU institutions',")
            console.log(json);
        })
        .catch(err => {
            console.error(err);
        });
}