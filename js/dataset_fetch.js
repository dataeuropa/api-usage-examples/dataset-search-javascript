function dataset_fetch() {


    // retrieve the information for a datasts in the catalogue 'EU institutions'
    // the dataset has the ID 'g3ebxpyzj2lulbjqwkhg'

    fetch("https://data.europa.eu/api/hub/search/datasets/g3ebxpyzj2lulbjqwkhg", {
        "method": "GET"
    })
        .then(async response => {
            const json = await response.json();

            console.log(json);
        })
        .catch(err => {
            console.error(err);
        });
}